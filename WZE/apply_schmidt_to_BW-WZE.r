#' Anwendung des Mortalitätsmodells von M.Schmidt auf den Daten der WZE
#' Baden-Württemberg.
#' Daten WZE-BW bereitgestellt durch Stefan Meining (Kontakt BU, FVA-BW)
#' Klimadaten BW bereitgestellt durch Thorsten Zeppenfeld, NW-FVA
#' Mortalitätsmodell bereitgestellt durch Matthias Schmidt (Autor), NW-FVA
#'
#' christian.vonderach@wwd.uni-freiburg.de
#' 04.07.2024
#'
rm(list=ls())
require(readxl)
require(dplyr)

# set path
if(Sys.info()[["nodename"]] == "IWW61"){
  p <- "C:/Users/Vonderach/Nextcloud/WEHAM20/"
} else {
  p <- "C:/BitBWCloud/WEHAM20/"
}

## Daten ####
### WZE-Daten BW ####
wze <- readxl::read_xlsx(file.path(p, "Daten/WZE/BW/Urdaten/Ch_Vonderach_2024.xlsx")) |>
  filter(!is.na(StpCounter)) |>
  as.data.frame()
head(wze)

aufn <- wze |>
  group_by(StpCounter, Satellit, Aufnahme) |>
  summarise(Aufnahme = mean(Aufnahme)) |>
  mutate(prevAuf = lag(Aufnahme, 1) - Aufnahme,
         prevAuf = ifelse(is.na(prevAuf), -1, prevAuf)) |>
  as.data.frame()
head(aufn)
summary(aufn$prevAuf)

w <- wze |>
  mutate(alter = BAlter,
         alter_corr = ifelse(alter > 150, 150, BAlter)) |>
  group_by(StpCounter, Satellit, Aufnahme) |>
  # remove Voraufnahme in case the "Normale Bonitur" is available in that year
  mutate(NV = length(unique(AufTyp))) |> # is 1 if only "N" or "V", is 2 if both "N" and "V" available
  filter(!(AufTyp == "V" & NV == 2)) |>
  ## check the number of trees per StpCounter and Aufnahme(jahr)
  group_by(StpCounter, Aufnahme) |>
  mutate(nObs = length(StpCounter)) |>
  ## calculate the number of species at each StpCounter, Satellit, Aufnahme
  group_by(StpCounter, Satellit, Aufnahme) |>
  mutate(n_baumart = length(unique(BArt_Name))) |>
  ## in case the previous year is not available, then the year of removal is not clear
  ## then draw a unit distributed random variable to set the year of removal
  group_by(StpCounter, Satellit, Aufnahme, BaumNr) |>
  left_join(aufn, by = c("StpCounter", "Satellit", "Aufnahme")) |>
  mutate(AusJahr = ifelse(prevAuf < -1,
                            Aufnahme - as.integer(runif(1, 1, max = abs(prevAuf))),
                            Aufnahme),
         ## select approriate reason of removal ####
         tod_bekannt = ifelse(ErsGrund %in% c(11, 12, 13, 14, 31, 32, 33, 41, 42, 43), TRUE, FALSE)) |>
  as.data.frame()

head(w)
View(subset(w, prevAuf < -1))


## Mortalitätsmodell Schmidt ####
load(file.path(p, "Daten/WZE/MortModellSchmidt/survival_models_fva_bw.RData"))
terms <- unique(c(attr(mod_k6all_tod_bekannt_red_Buche$terms, "term.labels"),
                  attr(mod_k6all_tod_bekannt_red_Eiche$terms, "term.labels"),
                  attr(mod_k6all_tod_bekannt_red_Fichte$terms, "term.labels"),
                  attr(mod_k6all_tod_bekannt_red_Kiefer$terms, "term.labels")))
terms <- terms[!(terms %in% c("alter", "alter_corr", "n_baumart"))]

### Klimadaten an WZE-Punkten BW ####
cl <- read.csv2(file.path(p, "Daten/WZE/BW/Urdaten/vonNW-FVA/klimavar_stp.csv"))
head(cl)
# rename columns
head(cl)
cl$tsum_1991_2020_corr <- cl$tsum_knp9120
cl$tsum_corr_detrend <- cl$tsum - cl$tsum_knp9120
cl$tsum_t1_corr_detrend <- cl$tsum_t1 - cl$tsum_knp9120
cl$tsum_t2_corr_detrend <- cl$tsum_t2 - cl$tsum_knp9120
cl$tsum_t3_corr_detrend <- cl$tsum_t3 - cl$tsum_knp9120
cl$tsum_t4_corr_detrend <- cl$tsum_t4 - cl$tsum_knp9120
cl$ari_1991_2020_corr <- cl$psum_knp9120 / cl$tsum_knp9120
cl$ari_corr_detrend <- (cl$psum / cl$tsum) - (cl$psum_knp9120 / cl$tsum_knp9120)
cl$ari_t4_corr_detrend <- (cl$psum_t4 / cl$tsum_t4) - (cl$psum_knp9120 / cl$tsum_knp9120)

cl <- cl[, c("STP", "refyr", "Aufnahme", terms)]

wc <- merge(w, cl, by.x = c("StpCounter", "Aufnahme"), by.y = c("STP", "Aufnahme"))

spp <- c(Bu="Buche", Ei="Eiche", Fi="Fichte", Ki="Kiefer")
for(i in seq(along=spp)){
  par(mfrow=c(1,1))
  # i <- 2
  ba <- spp[i]
  wcb <- subset(wc, BArt_Name == names(ba))
  nrow(wcb)
  nrow(wcb[wcb$tod_bekannt==TRUE,])
  eval(parse(text=paste0("obj <- mod_k6all_tod_bekannt_red_", ba)))
  wcb$yhat <- predict(obj, newdata = wcb, type = "response")
  hist(yhat, main = paste0("BW: ", ba))
  hist(obj$fitted.values, main=paste("NW-FVA: ", ba))
  tmp <- summary(wcb$yhat)
  boxplot(yhat ~ tod_bekannt, data=wcb, ylim=c(0, 2*tmp[[5]]), notch=TRUE,
          main=paste0("BW: ", ba))
  # new fit
  obj2 <- update(obj, data=wcb)
  par(mfrow=c(3,3))
  plot(obj2)
  rm(obj)
}
