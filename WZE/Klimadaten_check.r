#' Check der Klimadaten für Mortalitätsmodelle Schmidt/NW-FVA
#' Klimadaten an den WZE-Koordinaten BW

rm(list=ls())
# set path
if(Sys.info()[["nodename"]] == "IWW61"){
  p <- "C:/Users/Vonderach/Nextcloud/WEHAM20/"
} else {
  p <- "C:/BitBWCloud/WEHAM20/"
}

## Mortalitätsmodell Schmidt ####
load(file.path(p, "Daten/WZE/MortModellSchmidt/survival_models_fva_bw.RData"))
terms <- unique(c(attr(mod_k6all_tod_bekannt_red_Buche$terms, "term.labels"),
                  attr(mod_k6all_tod_bekannt_red_Eiche$terms, "term.labels"),
                  attr(mod_k6all_tod_bekannt_red_Fichte$terms, "term.labels"),
                  attr(mod_k6all_tod_bekannt_red_Kiefer$terms, "term.labels")))
terms <- terms[!(terms %in% c("alter", "alter_corr", "n_baumart"))]

## Klimadaten ####
df <- read.csv2(file.path(p, "Daten/WZE/BW/Urdaten/vonNW-FVA/klimavar_stp.csv"))
head(df)
df$tsum_1991_2020_corr <- df$tsum_knp9120
df$tsum_corr_detrend <- df$tsum - df$tsum_knp9120
df$tsum_t1_corr_detrend <- df$tsum_t1 - df$tsum_knp9120
df$tsum_t2_corr_detrend <- df$tsum_t2 - df$tsum_knp9120
df$tsum_t3_corr_detrend <- df$tsum_t3 - df$tsum_knp9120
df$tsum_t4_corr_detrend <- df$tsum_t4 - df$tsum_knp9120
df$ari_1991_2020_corr <- df$psum_knp9120 / df$tsum_knp9120
df$ari_corr_detrend <- (df$psum / df$tsum) - (df$psum_knp9120 / df$tsum_knp9120)
df$ari_t4_corr_detrend <- (df$psum_t4 / df$tsum_t4) - (df$psum_knp9120 / df$tsum_knp9120)

df <- df[, c("STP", "refyr", "Aufnahme", terms)]


pdf(file.path(p, "Daten/WZE/BW/Ergebnisse/Klimadaten-hist.pdf"))
for(i in 4:ncol(df)){
  # i <- 4
  cn <- colnames(df)[i]
  ## add in range of this climate variable for different species
  lgnd <- FALSE
  ll <- list()
  if(TRUE){
    spp <- c("Buche", "Eiche", "Fichte", "Kiefer")
    ylwr <- c(0, 0.05, 0.1, 0.15)
    yupr <- c(0.05, 0.1, 0.15, 0.2)
    for(j in seq(along=spp)){
      # j <- 1
      ba <- spp[j]
      eval(parse(text = paste0("obj <- mod_k6all_tod_bekannt_red_", tolower(ba), "_data.summary")))
      idx <- grep(cn, colnames(obj))
      if(length(idx)>0){
        lgnd <- TRUE
        tmp <- obj[, idx]
        min <- tmp[grepl("Min", tmp)]
        min <- gsub("Min. ", replacement = "", min)
        min <- gsub(":", replacement = "", min)
        min <- as.numeric(gsub(" ", replacement = "", min))
        max <- tmp[grepl("Max", tmp)]
        max <- gsub("Max. ", replacement = "", max)
        max <- gsub(":", replacement = "", max)
        max <- as.numeric(gsub(" ", replacement = "", max))
        # abline(v=c(min, max), col=j, lwd=2)
        ll[[j]] <- c(min, max)
      } else {
        ll[[j]] <- c(NA, NA)
      }
    }
    xmin <- min(sapply(1:4, function(a) ll[[a]][1]), na.rm=TRUE)
    xmax <- min(sapply(1:4, function(a) ll[[a]][2]), na.rm=TRUE)
  } else {
    xmin <- xmax <- NA
  }
  h <- hist(df[,i], main=paste0("Klimadaten BW: ", cn),
            xlab=cn, xlim = range(c(xmin, xmax, df[, i]), na.rm = TRUE))
  rug(df[,i])
  abline(v=c(mean(df[, i], na.rm=TRUE), median(df[,i], na.rm = TRUE)),
         lty=2:3, lwd=2)
  text(x=min(h$mids), y=max(h$counts), adj = c(0, 1),
       labels = paste0("#NA = ", length(which(is.na(df[,i])))))
  legend("right", legend = c("mean", "median"), lty=2:3, lwd=2)
  for(j in 1:4){
    if(length(ll)>0){
      if(!is.null(ll[[j]])){
        (tmpcol <- col2rgb(j, FALSE))
        rect(xleft = ll[[j]][1], ybottom = ylwr[j] * max(h$counts),
             xright = ll[[j]][2], ytop = yupr[j] * max(h$counts),
             col=rgb(tmpcol[1,1], tmpcol[2,1], tmpcol[3,1], 255*0.8, maxColorValue = 255),
             border = NA)
      }
    }
  }
  if(lgnd){
    legend("topright", legend = spp, col=seq(along=spp), lwd=7,
           title = "Model range")
  }

}
dev.off()
